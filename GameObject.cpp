#include "GameObject.hpp"

// CONSTRUCTORS AND DESTRUCTORS

GameObject::GameObject(Point* p, string path)
{
	position = p;
	texture.loadFromFile(path);
}

GameObject::GameObject(Point* p, string path, int x, int y, int width, int height)
{
	position = p;
	texture.loadFromFile(path, sf::IntRect(x, y, width, height));
}

GameObject::~GameObject()
{
	delete position;
}

// GETTERS AND SETTERS

Point* GameObject::getPosition()
{
	return position;
}

void GameObject::setPosition(Point* p)
{
	if(position != NULL)
		delete position;

	position = p;
}

// OTHER FUNCTIONS

string GameObject::toString()
{
	return position->toString();
}