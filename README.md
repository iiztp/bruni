# BRUni

A simple BR Game for a project in a uni course.

Manuel d'utilisation du jeu

## Compiling

### Compiling all of the project

To compile you must have the following dependencies :
- g++ : The C++ compiler
- libsfml-dev : The graphical library for the game
- libmysqlcppconn-dev : The library of mysql database for C++

A simple command to install all of those dependencies at once would be : 
```apt-get install g++ libmysqlcppconn-dev libsfml-dev```

and then do the ```make``` command in the repository freshly cloned.

### Compiling errors with mysql

If you have compiling errors with mysql, you can disable the multiplayer to compile the whole project.
You just need to comment the concerned lines in menu.cpp and compile without the -lmysqlcppconn option and the file "multigame.cpp"

The concerned lines are from 75 to 98, you also need to comment the include with multigame in the header file.

## User manual

Shoot with left click
Move with zqsd

(z : move up,
s : move down,
q : move left,
d : move right)

The goal is to be the last alive.
The username is your uuid (Unique User Id) for the file savings.

## Multiplayer issues

As we are using a database, you need to completely wait for the first user to finish his loading
before joining him, else the database will be compromised. 
If the database is compromised do ```make cleandb && ./cleandb``` it should write "success" if not, see 'Compiling errors with mysql'.

## Authors

@TimLim1590 : Thibaud Limbach

@corentin_santoro : Corentin Santoro

@iiztp : Matthieu Amet
