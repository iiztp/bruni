#ifndef POINT_H
#define POINT_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <math.h>

using namespace std;

class Point

{
    public:

        /************************************/
        /*   Constructors and destructors   */
        /************************************/

        /**
         * Constructor of Point 
         */
        Point();

        Point(Point* p);

        /**
         * Constructor of Point 
         * \param int x
         * \param int y
         */
        Point(int x, int y);

        /**
         * Destructor of the Point 
         */
        virtual ~Point();

        /************************************/
        /*        Getters and setters       */
        /************************************/

        /**
         * Returns abscissa
         * \return int x
         */
        int getX();

        /**
         * Returns ordinate
         * \return int y
         */
        int getY();

        /**
         * Set x
         * \param int newX
         */
        void setX(int newX);

        /**
         * Set y
         * \param int newY
         */
        void setY(int newY);

        /************************************/
        /*          Other functions         */
        /************************************/

        /**
         * Move the point, add changeX and changeY to x and y
         */
        void move(int changeX, int changeY);

        /**
         * Returns distance between two points
         * \return int distance between two points
         */
        int distance(Point* p);

        /**
         * Displays the point in the console
         * \return string 
         */
        string toString();

        /************************************/
        /*         Overload functions       */
        /************************************/

        /**
         * Checks if two points are equals
         * \param Point* b : the point to check
         * \return true if equals
         */
        bool operator==(Point* b);

    private:
        int x;
        int y;
    
};

#endif //POINT_H