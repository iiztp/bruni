#include "Game.hpp"

Game::Game(sf::RenderWindow* window)
{
    this->window = window;
	this->map = new Map(MAP_SIZE,MAP_SIZE);
}

Game::Game(Map* m, sf::RenderWindow* window)
{
    this->window = window;
    this->map = m;
}

Game::~Game()
{
	delete(this->map);
	delete(this->player);
}

void Game::handleEvents()
{
    while(window->pollEvent(event))
    {
        switch (event.type)
        {
            // window closed
            case sf::Event::Closed:
                window->close();
                break;
            // key pressed
            case sf::Event::KeyPressed:
                int movement = player->getMovement();
                int x = player->getX();
                int y = player->getY();
                switch(event.key.code)
                {
                    case sf::Keyboard::Q:
                        player->setWay('Q');
                        break;
                    case sf::Keyboard::D:    
                        player->setWay('D');
                        break;
                    case sf::Keyboard::Z:
                        player->setWay('Z');
                        break;
                    case sf::Keyboard::S:                            
                        player->setWay('S');
                        break;
                    default:
                        player->setWay('\n');
                }

                if(!this->mapCollision(player))
                {
                    player->move();
                }
                break;
        }

        if(event.type == sf::Event::MouseButtonPressed)
        {
            if(event.mouseButton.button == sf::Mouse::Left)
                if(player->isVisible())
                    player->shoot();
        }
    }
}

Player* Game::getPlayer()
{
	return this->player;
}

Map* Game::getMap()
{
	return this->map;
}

sf::RenderWindow* Game::getWindow()
{
    return this->window;
}

bool Game::mapCollision(Sprite* s)
{
	int x = s->getX();
    int y = s->getY();
    int width = s->getWidth();
    int height = s->getHeight();

    switch(s->getWay())
    {
        case 'Q':
        x -= s->getMovement();
        break;
        case 'D':
        x += s->getMovement();
        break;
        case 'Z':
        y -= s->getMovement();
        break;
        case 'S':
        y += s->getMovement();
        break;
    }

    int column = x / TEXTURE_SIZE;
    int row = y / TEXTURE_SIZE;

    if(getMap()->isWall(column, row))
        return true;

    column = (x + width - 1) / TEXTURE_SIZE;

    if(getMap()->isWall(column, row))
        return true;

    row = (y + height - 1) / TEXTURE_SIZE;

    if(getMap()->isWall(column, row))
        return true;

    column = x / TEXTURE_SIZE;

    if(getMap()->isWall(column, row))
        return true;

    return false;
}

bool Game::isCollision(Sprite* s1, Sprite* s2)
{
	if(s1->isVisible() && s2->isVisible())
		if(s1->getX() >= s2->getX())
	        if(s1->getY() >= s2->getY())
	            if((s1->getX()+s1->getWidth()) <= (s2->getX()+s2->getWidth()))
	                if((s1->getY()+s1->getHeight()) <= (s2->getY()+s2->getHeight()))
	                    return true;

    return false;
}