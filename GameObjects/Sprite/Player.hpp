#ifndef PLAYER_H
#define PLAYER_H

#include "../Sprite.hpp"
#include "Bullet.hpp"

class Bullet;

using namespace std;

class Player : public Sprite
{
	public:

            /************************************/
            /*   Constructors and destructors   */
            /************************************/

		/**
             * Constructor of Player 
             * \param string playerUUID
             * \param Point* pos
             * \param int x
             * \param int y
             * \param int width
             * \param int height
             */
            Player(string playerUUID, Point* pos, int x, int y, int width, int height);

		/**
             * Destructor of the player 
             */
		virtual ~Player();

            /************************************/
            /*        Getters and setters       */
            /************************************/

            /**
             * Return the player kill number
             * \return int
             */
            int getKills();

            /**
             * Return the player damages
             * \return int
             */
            int getDamages();

            /**
             * Return the player experience
             * \return int
             */
            int getExperience();

            /**
             * Return the player level 
             * \return int level
             */
            int getLevel();

            /**
             * Return the player level 
             * \return int life 
             */
            int getLife();

            /**
             * Return the player defense
             * \return int defense
             */
            int getDefense();
            /**
             * Returns the player attack 
             * \return int attack
             */
            int getAttack();
            /**
             * Returns the player bullet 
             * \return Bullet* bullet
             */
            Bullet* getBullet();

            /**
             * Returns the player uuid 
             * \return string uuid
             */
            string getUuid();

            /**
             * Return the player point level 
             * \return int 
             */
            int getPointLevel();

            /**
             * Return the player point defense 
             * \return int 
             */
            int getPointDefense();

            /**
             * Return the player point attack 
             * \return int 
             */
            int getPointAttack();

            /**
             * Return the player point life 
             * \return int 
             */
            int getPointLife();

            /**
             * Set the player experience
             * \param int xp  
             */
            void setExperience(int xp);

            /**
             * Set the player level
             * \param int level 
             */
            void setLevel(int lvl);

            /**
             * Set the player life
             * \param int life  
             */
            void setLife(int life);

            /**
             * Set the uuid of the player
             * \param string uuid 
             */
            void setUuid(string uuid);

            /**
             * Set the defense of the player  
             * \param int def 
             */
            void setDefense(int def);

            /**
             * Set the player point level 
             * \param int nb
             */
            void setPointLevel(int nb);

            /**
             * Set the player point defense 
             * \param int nb
             */
            void setPointDefense(int nb);

            /**
             * Set the player point attack 
             * \param int nb
             */
            void setPointAttack(int nb);

            /**
             * Set the player point life 
             * \param int nb
             */
            void setPointLife(int nb);

            /************************************/
            /*             Updaters             */
            /************************************/

		/**
             * Levels up the player  
             */
		void levelUp();

            /**
             * Update the number of kills  
             */
            void updateKills();

            /**
             * Update the number of damages  
             */
            void updateDamages();

		/**
             * Updates the player life
             * \param int damages 
             */
		void updateLife(int damages);

		/**
             * Updates the player experience
             * \param int xp  
             */
		void updateExperience(int xp);

            /**
             * Update the player life, defense and attack according to point life, point defense and point attack
             */
            void updateStat();

            /************************************/
            /*         Other functions          */
            /************************************/

            /**
             * Display the Player on sfml window on position (x,y)
             * \param sf::RenderWindow* window
             * \param int x
             * \param int y
             */
            void display(sf::RenderWindow* window, int x, int y);

		/**
             * Returns true if the player is alive
             * \return bool if the player is alive
             */
		bool isAlive();

            /**
             * Allows the player to shoot
             */
            void shoot();


            /**
             * Walking animation
             */
            void animX();

            /**
             * Direction animation
             */
            void animY();

            /**
             * Displays the Player in the console
             * \return string 
             */
            string toString();

	protected:
            string uuid;
            int level = PLAYER_LEVEL;
            int experience = PLAYER_EXPERIENCE;
            int life = PLAYER_LIFE;
            int defense = PLAYER_DEFENSE;
            Bullet* bullet;
            int nbKills = 0;
            int nbDamages = 0;
            int pointLevel = 0;
            int pointDefense = 0;
            int pointAttack = 0;
            int pointLife = 0;
};

#endif //PLAYER_H