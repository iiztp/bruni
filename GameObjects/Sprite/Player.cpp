#include "Player.hpp"

// CONSTRUCTORS AND DESTRUCTORS

Player::Player(string playerUUID, Point* pos, int x, int y, int width, int height) : Sprite(PLAYER_SIZE, PLAYER_SIZE, PLAYER_MOVEMENT, true, pos, TEXTURE_PLAYER, x, y, width, height)
{
   life = PLAYER_LIFE;
   experience = PLAYER_EXPERIENCE;
   level = PLAYER_LEVEL;
   defense = PLAYER_DEFENSE;
   bullet = new Bullet(BULLET_RANGE, BULLET_ATTACK, this, TEXTURE_BULLET);
   uuid = playerUUID;
}

Player::~Player()
{
   delete bullet;
}

// GETTERS AND SETTERS

int Player::getPointLevel()
{
   return pointLevel;
}

int Player::getPointDefense()
{
   return pointDefense;
}

int Player::getPointAttack()
{ 
   return pointAttack;
}

int Player::getPointLife()
{ 
   return pointLife;
}

int Player::getLevel()
{
   return level;
}

int Player::getKills()
{
   return nbKills;
}

int Player::getDamages()
{
   return nbDamages;
}

int Player::getLife()
{
   return life;
}

int Player::getDefense()
{
   return defense;
}

int Player::getExperience()
{
   return experience;
}

string Player::getUuid()
{
   return uuid;
}

int Player::getAttack()
{
   return bullet->getAttack();
}

Bullet* Player::getBullet()
{
   return bullet;
}

void Player::setPointLevel(int nb)
{
   pointLevel = nb;
}

void Player::setPointDefense(int nb)
{
   pointDefense = nb;
}

void Player::setPointAttack(int nb)
{
   pointAttack = nb;
}

void Player::setPointLife(int nb)
{
   pointLife = nb;
}

void Player::setLife(int life)
{
   this->life = life;
}

void Player::setLevel(int lvl)
{
   level = lvl;
}

void Player::setUuid(string uuid)
{
   this->uuid = uuid;
}

void Player::setDefense(int def)
{
   defense = def;
}

void Player::setExperience(int xp)
{
   experience = xp;
}

// UPDATERS

void Player::levelUp()
{
   level++;
   pointLevel++;
}

void Player::updateLife(int damages)
{
   srand(time(0));
   int random = rand()%defense;
   if(random <= bullet->getAttack())
      life -= damages;
}

void Player::updateExperience(int xp)
{
   experience += xp;
}

void Player::updateKills()
{
   nbKills++;
}

void Player::updateDamages()
{
   nbDamages += bullet->getAttack();
}

void Player::updateStat()
{
   defense = PLAYER_DEFENSE + pointDefense;
   bullet->setAttack(bullet->getAttack() + pointAttack);
   life = PLAYER_LIFE + UPDATE_LIFE*pointLife;
}

// OTHER FUNCTIONS

void Player::display(sf::RenderWindow* window, int x, int y)
{
   if(visible)
   {
      sf::RectangleShape rectangle(sf::Vector2f((int)(getLife()*0.32), 3));
      rectangle.setPosition(getX(), getY()-5);
      rectangle.setFillColor(sf::Color(255, 0, 0));
      rectangle.setOutlineThickness(1);
      rectangle.setOutlineColor(sf::Color(0, 0, 0));

      sprite.setTextureRect(sf::IntRect(x, y, width,height));
      window->draw(sprite);
      window->draw(rectangle);
   }
}

bool Player::isAlive()
{
   return life > 0;
}

void Player::shoot()
{
   if(bullet->isVisible())
      return;

   bullet->setVisible(true);
   bullet->setWay(getWay());
}

void Player::animX()
{
   if(getAnimX() < 2*TEXTURE_SIZE)
   {
      setAnimX(getAnimX() + TEXTURE_SIZE);
   } else {
      setAnimX(0);
   }
}

void Player::animY()
{
   switch(getWay())
   {
      case 'S':
         setAnimY(0);
         break;
      case 'D':
         setAnimY(2 * TEXTURE_SIZE);
         break;
      case 'Q':
         setAnimY(1 * TEXTURE_SIZE);
         break;
      case 'Z':
         setAnimY(3 * TEXTURE_SIZE);
         break;
   }
}

string Player::toString()
{
   return NULL;
}
