#ifndef BULLET_HPP
#define BULLET_HPP

#include "../Sprite.hpp"
#include "Player.hpp"

class Player;

class Bullet : public Sprite
{
	public:
        /************************************/
        /*   Constructors and destructors   */
        /************************************/

        /**
        * Constructor of Bullet
        * \param int range
        * \param int attack
        * \param Player* player
        * \param string path
        */
		Bullet(int range, int attack, Player* player, string path);

        /**
        * Destructor of the bullet
        */
		~Bullet();

        /************************************/
        /*       Getters and setters        */
        /************************************/

        /**
        * Returns the attack of the bullet
        * \return int attack
        */
        int getAttack();

        /**
        * Returns the Bullet range 
        * \return int attack
        */
        int getRange();

        /**
        * Return the player belonging to the bullet
        * \return Player* 
        */
        Player* getPlayer();

    	/**
        * Set the attack of the Bullet  
        * \param int atk 
        */
    	void setAttack(int atk);

        /**
         * Set the range of the Bullet  
         * \param int atk 
         */
        void setRange(int range);

        /************************************/
        /*         Other functions          */
        /************************************/

        /**
        * Reinit the bullet to the player's place 
        */
        void reinit();

        /** 
        * Move the bullet 
        */
        void move();

    	/**
        * Displays the Bullet in the console
        * \return string 
        */
        string toString();

	private:
    	int range;
    	int attack;
        int rangeMax;
        Player* player;
};

#endif //BULLET_HPP