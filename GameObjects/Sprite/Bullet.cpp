#include "Bullet.hpp"

// CONSTRUCTORS AND DESTRUCTORS

Bullet::Bullet(int range, int attack, Player* player, string path) : Sprite(BULLET_SIZE, BULLET_SIZE, BULLET_MOVEMENT, false, new Point(player->getX()-(player->getHeight()/2), player->getY()-(player->getWidth()/2)), path)
{
	this->rangeMax = range;
	this->range = range*TEXTURE_SIZE;
	this->attack = attack;
	this->player = player;
}

Bullet::~Bullet()
{

}

// GETTERS

int Bullet::getRange()
{
	return range;
}

Player* Bullet::getPlayer()
{
	return player;
}

int Bullet::getAttack()
{
	return attack;
}

// SETTERS

void Bullet::setAttack(int atk)
{
	attack = atk;
}

void Bullet::setRange(int range)
{
	this->range = range;
}

// OTHER FUNCTIONS

void Bullet::reinit()
{
	setVisible(false);
	delete position;
	position = new Point(player->getX()+(player->getHeight()/2), player->getY()+(player->getWidth()/2));
	range = rangeMax*TEXTURE_SIZE;
}

void Bullet::move()
{
	if(isVisible())
	{
		range -= BULLET_MOVEMENT;
		if(range <= 0)
		{
			reinit();
		}
		Sprite::move();
	}
	else
	{
		position->setX(player->getX()+(player->getWidth()/2));
		position->setY(player->getY()+(player->getHeight()/2));
		setWay(player->getWay());
	}
	getSprite().setPosition(getPosition()->getX(), getPosition()->getY());
}

string Bullet::toString()
{
	return "Bullet : attack = " + to_string(attack) + " & range = " + to_string(range);
}