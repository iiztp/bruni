#include "Map.hpp"

// CONSTRUCTORS AND DESTRUCTORS

Map::Map(int x, int y) : GameObject(new Point(x, y), TEXTURE_MAP)
{
	table = new int*[x];

	for(int i = 0; i < x; i++)
	{
		table[i] = new int[y];
	}

	loadTextures(TEXTURE_MAP);
}

Map::Map(Point* pos) : GameObject(pos, TEXTURE_MAP)
{
	table = new int*[pos->getX()];

	for(int i = 0; i < pos->getX(); i++)
	{
		table[i] = new int[pos->getY()];
	}

	loadTextures(TEXTURE_MAP);
}

Map::~Map()
{
	for(int i = 0; i < getX(); i++)
	{
		delete [] table[i];
	}

	delete [] table;
}

// GETTERS AND SETTERS

int Map::getValue(int x, int y)
{
	return table[x][y];
}

int** Map::getMap()
{
	return table;
}

int Map::getX()
{
	return position->getX();
}

int Map::getY()
{
	return position->getY();
}

void Map::setInt(int i, int j, int newInt)
{
	if(i < 0 || i >= getX())
		throw string("i is out of border");
	if(j < 0 || j >= getY())
		throw string("j is out of border");

	table[i][j] = newInt;
}

void Map::changeInt(int before, int after)
{
	for(int i = 0; i < getX(); i++)
	{
		for(int j = 0; j < getY(); j++)
		{
			if(table[i][j] == before)
				table[i][j] = after;
		}
	}
}

// GENERATION ALGORITHMS

bool Map::isEffective()
{
	for(int i = 1; i < getX()-1; i++)
	{
		bool isEffective = false;

		for(int j = 0; j < getY(); j++)
		{
			if(table[i][j] == 0) isEffective = true;
		}

		if(!isEffective) return false;
	}

	for(int j = 1; j < getY()-1; j++)
	{
		bool isEffective = false;
		for(int i = 0; i < getX(); i++)
		{
			if(table[i][j] == 0) isEffective = true;
		}

		if(!isEffective) return false;
	}

	return true;
}

void Map::generate()
{
	srand(time(0));

	for(int i = 0; i < getX(); i++)
	{
		for(int j = 0; j < getY(); j++)
		{
			table[i][j] = 0;

			if(i==0||i==getX()-1||j == 0||j==getY()-1)table[i][j]=1;
		}
	}

	for(int i = 2; i < getX()-2; i++)
	{
		for(int j = 2; j < getY()-2; j++)
		{
			int last = -1;
			int currentX = i;
			int currentY = j;
			while((countWallsAround(currentX, currentY) < 2))
			{
				int r = rand()%4;

				while(r%2 == last-1 || (rand()%2 == 1 && r%2 == last)) r = rand()%4;

				switch(rand() % 4)
				{
					case 0:
						currentX++;
						break;
					case 1:
						currentX--;
						break;
					case 2:
						currentY++;
						break;
					case 3:
						currentY--;
						break;
				}

				if((currentX < getX()-1) && (currentY < getY()-1) && currentX > 0 && currentY > 0) table[currentX][currentY] = 1;
				else break;

				last = r%2;
			}
		}
	}

	for(int i = 1; i < getX()-1; i++)
	{
		for(int j = getY()-2; j > 0; j--)
		{
			if(isWall(i, j)) deleteAround(i, j);
		}
	}
}

void Map::generateEffective()
{
	do
	{
		generate();
	}while(!isEffective());
}

// OTHER FUNCTIONS

void Map::loadTextures(string texturePath)
{
	texture.loadFromFile(texturePath);
	tile[0].setTexture(texture);
	tile[0].setTextureRect(sf::IntRect(0, 0, TEXTURE_SIZE, TEXTURE_SIZE));

	tile[1].setTexture(texture);
	tile[1].setTextureRect(sf::IntRect(288, 0, TEXTURE_SIZE, TEXTURE_SIZE));
}

bool Map::isWall(int x, int y)
{
	return (x < 0 || x >= getX() || y < 0 || y >= getY()) || (table[x][y] == 1);
}

int Map::countWallsAround(int x, int y)
{
	int nbOfWallsAround = 0;

	if(isWall(x+1, y)) nbOfWallsAround++;
	if(isWall(x-1, y)) nbOfWallsAround++;
	if(isWall(x, y+1)) nbOfWallsAround++;
	if(isWall(x, y-1)) nbOfWallsAround++;

	return nbOfWallsAround;
}

void Map::deleteAround(int x, int y)
{
	if(isWall(x+1, y+1) && (x+1) < (getX()-1) && (y+1) < (getY()-1)) table[x+1][y+1] = 0;
	if(isWall(x-1, y+1) && (x-1) > 0 && (y+1) < (getY()-1)) table[x-1][y+1] = 0;
	if(isWall(x+1, y-1) && (x+1) < (getX()-1) && (y-1) > 0) table[x+1][y-1] = 0;
	if(isWall(x-1, y-1) && (x-1) > 0 && (y-1) > 0) table[x+1][y+1] = 0;
}

void Map::display(sf::RenderWindow* window)
{
	for(int i = 0; i < getX(); i++)
	{
		for(int j = 0; j < getY(); j++)
		{
			if(isWall(i, j))
			{
				tile[1].setPosition(i*TEXTURE_SIZE, j*TEXTURE_SIZE);
				window->draw(tile[1]);
			} else {
				tile[0].setPosition(i*TEXTURE_SIZE, j*TEXTURE_SIZE);
				window->draw(tile[0]);
			}
		}
	}
}

void Map::displayRaw()
{
	for(int j = 0; j < getY(); j++)
	{
		for(int i = 0; i < getX(); i++)
		{
			cout << table[i][j];
		}
		cout << endl;
	}

	cout << endl << endl;
}