#ifndef MAP_H
#define MAP_H

#include "../GameObject.hpp"
#include <fstream>
#include <ctime>
#include <algorithm>

using namespace std;

class Map : public GameObject
{
	public:
		/************************************/
        /*   Constructors and destructors   */
        /************************************/

		/**
		 * Constructor of Map with x column and y line 
		 * \param int x
		 * \param int y
		 */
		Map(int x, int y);

		/**
		 * Constructor of Map with x column and y line 
		 * \param Point* pos
		 */
		Map(Point* pos);

		/**
		 * Destructor of Map
		 */
		virtual ~Map();

		/************************************/
        /*   	  Getters and setters       */
        /************************************/

        /**
		 * Get map in a 2 dimensions table
		 */
		int** getMap();

		/**
		 * Get the number of lines
		 */
		int getY();

		/**
		 * Get the number of columns
		 */
		int getX();

		/**
		 * Return the value of Map on the point (x,y)
		 * \param int x
		 * \param int y 
		 * \return int 
		 */
		int getValue(int x, int y);

		/**
         * Replaces one int in the position "i,j"
         * \param int i : line number
         * \param int j : column number
         * \param int newInt
         */
		void setInt(int i, int j, int newInt);

		/**
		 * Change all of the occurrences of an int to another int
		 * \param int before : old int
		 * \param int after : new int
		 */
		void changeInt(int before, int after);


		/************************************/
        /*   	Generation algorithms       */
        /************************************/

		/**
		 * Check if the map is correct
		 */
		bool isEffective();

		/**
		 * Generates a random map
		 */
		void generate();
		
		/**
		 * Generates a random correct map
		*/
		void generateEffective();

		/************************************/
        /*       	Other functions         */
        /************************************/

		/**
		 * Checks if there is a wall on the point (x,y)
		 * \param int x
		 * \param int y 
		 */
		bool isWall(int x, int y);

		/**
		 * Get the number of walls linearly to the point (x, y)
		 * \param int x
		 * \param int y 
		 */
		int countWallsAround(int x, int y);

		/**
		 * Load textures from file
		 * \param string texturePath
		 */
		void loadTextures(string texturePath);

		/**
		 * Remove the wall around a box if necessary
		 * \param int x
		 * \param int y 
		 */
		void deleteAround(int x, int y);

		/**
		 * display the map on sfml window
		 * \param sf::RenderWindow* window
		 */
		void display(sf::RenderWindow* window);

		/**
		 * display the map on standard output
		 */
		void displayRaw();


	private:
		void loadTextures();
		int** table;
		sf::Sprite tile[2];
};

#endif