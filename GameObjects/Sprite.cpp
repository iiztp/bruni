#include "Sprite.hpp"

// CONSTRUCTORS AND DESTRUCTORS

Sprite::Sprite(int height, int width, int movement, bool visible, Point* pos, string path) : GameObject(pos, path)
{
	this->height = height;
	this->width = width;
	this->movement = movement;
	this->visible = visible;
	sprite.setTexture(this->texture);
	sprite.setPosition(pos->getX(), pos->getY());
	way = 'Z';
	anmX = 0;
	anmY = 0;
}

Sprite::Sprite(int height, int width, int movement, bool visible, Point* pos, string path, int x, int y, int w, int h) : GameObject(pos, path, x, y, w, h)
{
	this->height = height;
	this->width = width;
	this->movement = movement;
	this->visible = visible;
	sprite.setTexture(this->texture);
	sprite.setPosition(pos->getX(), pos->getY());
	way = 'Z';
	anmX = 0;
	anmY = 0;
}

Sprite::~Sprite()
{
	
}

// GETTERS AND SETTERS

int Sprite::getWidth()
{
	return width;
}

int Sprite::getHeight()
{
	return height;
}

int Sprite::getMovement()
{
	return movement;
}

int Sprite::getX()
{
	return position->getX();
}

int Sprite::getY()
{
	return position->getY();
}

char Sprite::getWay()
{
	return way;
}

int Sprite::getAnimX()
{
	return anmX;
}

int Sprite::getAnimY()
{
	return anmY;
}

void Sprite::setWidth(int w)
{
	width = w;
}

void Sprite::setHeight(int h)
{
	height = h;
}

void Sprite::setVisible(bool v)
{
	visible = v;
}

void Sprite::setMovement(int m)
{
	movement = m;
}

void Sprite::setSpritePosition(int x, int y)
{
	sprite.setPosition(sf::Vector2f(x, y));
}

void Sprite::setWay(char newWay)
{
	way = newWay;
}

void Sprite::setAnimX(int nb)
{
	anmX = nb;
}

void Sprite::setAnimY(int nb)
{
	anmY = nb;
}

sf::Sprite Sprite::getSprite()
{
	return sprite;
}

// OTHER FUNCTIONS

bool Sprite::isVisible() const
{
	return visible;
}

void Sprite::animX() {}
void Sprite::animY() {}

void Sprite::move()
{
	if(this->way == '\n') return;
	
	switch(this->way)
	{
		case 'Q':
		case 'q':
			getPosition()->move(-movement, 0);
		    break;
       	case 'D':
       	case 'd':
       		getPosition()->move(movement, 0);
       		break;
       	case 'Z':
       	case 'z':
       		getPosition()->move(0, -movement);
       		break;
       	case 'S':
       	case 's':     
       		getPosition()->move(0, movement);	
       		break;
	}
	animX();
    animY();
	sprite.setPosition(getPosition()->getX(), getPosition()->getY());
}

void Sprite::display(sf::RenderWindow* window, int x, int y)
{
	if(visible)
	{
		sprite.setTextureRect(sf::IntRect(x, y, width,height));
		window->draw(sprite);
	}
}

string Sprite::toString()
{
	return NULL;
}