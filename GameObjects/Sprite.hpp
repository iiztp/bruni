#ifndef SPRITE_H
#define SPRITE_H

#include "../GameObject.hpp"

using namespace std;

class Sprite : public GameObject
{
	public:

		/************************************/
        /*   Constructors and destructors   */
        /************************************/

		/**
		 * Constructor of Sprite
		 * \param int height
		 * \param int width
		 * \param int movement
		 * \param bool visible
		 * \param Point* pos
		 * \param string path 
		 */
		Sprite(int height, int width, int movement, bool visible, Point* pos, string path);

		/**
		 * Constructor of Sprite
		 * \param int height
		 * \param int width
		 * \param int movement
		 * \param bool visible
		 * \param Point* pos
		 * \param string path 
		 * \param int x 
		 * \param int y 
		 * \param int w 
		 * \param int h 
		 */
		Sprite(int height, int width, int movement, bool visible, Point* pos, string path, int x, int y, int w, int h);

		/**
		 * Destructor of Sprite
		 */
		virtual ~Sprite();

		/************************************/
        /*        Getters and setters       */
        /************************************/

		/**
		 * Returns height
		 * \return int
		 */
		int getHeight();

		/**
		 * Returns width
		 * \return int
		 */
		int getWidth();

		/**
		 * Returns movement
		 * \return int
		 */
		int getMovement();

		/**
		 * Return the direction of the sprite
		 * \return char way
		 */
		char getWay();

		/**
		 * Return the player abscissa
		 * return int
		 */
		int getX();

		/**
		 * Return the player ordinate
		 * \return int
		 */
		int getY();

		/**
		 * Return anmX
		 * \return anmX
		 */
		int getAnimX();

		/**
		 * Return anmY
		 * \return anmY
		 */
		int getAnimY();

		/*
		 * Return the sfml sprite
		 * \return sh::Sprite
		 */
		sf::Sprite getSprite();

		/**
		 * Set width
		 * \param int w
		 */
		void setWidth(int w);

		/**
		 * Set height
		 * \param int h
		 */
		void setHeight(int h);

		/**
		 * Set visible
		 * \param bool v
		 */
		void setVisible(bool v);

		/**
		 * Set movement
		 * \param int movement
		 */
		void setMovement(int m);

		/**
		 * Set the direction of sprite
		 * \param char way
		 */
		void setWay(char newWay);

		/**
		 * Set anmX
		 * \param int nb
		 */
		void setAnimX(int nb);

		/**
		 * Set anmY
		 * \param int nb
		 */
		void setAnimY(int nb);

		/**
		* Changes the sfml sprite position to the new coordinates
		* \param int x
		* \param int y
		*/
		void setSpritePosition(int x, int y);

		/**
		 * Returns visible
		 * \return bool
		 */
		bool isVisible() const;

		/**
		 * Move the sprite
		 */
		void move();

		/**
		 * Displays the Sprite on SFML window
		 */
		void display(sf::RenderWindow* window, int x, int y);

		/**
		 * Sprite animation on sfml window
		 */
		virtual void animX();

		/**
		 * Sprite animation on sfml window
		 */
		virtual void animY();

		/**
		 * Displays the Sprite in the consol
		 * \return string 
		 */
		virtual string toString()=0;


	protected:
		int height;
		int width;
		int movement;
		bool visible;
		char way;
		sf::Sprite sprite;

		int anmX;
		int anmY;

};

#endif //SPRITE_H