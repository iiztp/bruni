#include "SingleGame.hpp"

SingleGame::SingleGame(string playerUUID, sf::RenderWindow* window) : Game(window)
{
    map->generateEffective();
	ai = new Player*[NB_ENEMIES];
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        ai[i] = NULL;
        ai[i] = new Player("", getFreePoint(), 288, 0, 96, 128);
    }

    Point* freePoint = getFreePoint();
    Player* playerFromFile = Parser::loadPlayer(playerUUID, freePoint);

    if(playerFromFile == NULL)  player = new Player(playerUUID, freePoint, 0, 0, 96, 128);
    else    player = playerFromFile;
}

SingleGame::SingleGame(Map* map, string playerUUID, sf::RenderWindow* window) : Game(map, window)
{
    ai = new Player*[NB_ENEMIES];
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        ai[i] = NULL;
        ai[i] = new Player("", this->getFreePoint(), 288, 0, 96, 128);
    }

    Point* freePoint = getFreePoint();
    Player* playerFromFile = Parser::loadPlayer(playerUUID, freePoint);

    if(playerFromFile == NULL)  player = new Player(playerUUID, freePoint, 0, 0, 96, 128);
    else player = playerFromFile;
}

SingleGame::~SingleGame()
{
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        delete ai[i];
    }
    delete [] ai;
}

Player** SingleGame::getAi()
{
    return ai;
}

void SingleGame::handleBulletMovement()
{
    player->getBullet()->move();
    if(mapCollision(player->getBullet()))
        player->getBullet()->reinit();
    
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        Player* ai = getAi()[i];
        ai->getBullet()->move();
        if(mapCollision(ai->getBullet()))
            ai->getBullet()->reinit();
    }
}

int SingleGame::getRank()
{
    int rank = NB_ENEMIES+1;
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        if(!ai[i]->isVisible())
            rank--;
    }

    return rank;
}

Point* SingleGame::getFreePoint()
{
    srand(time(0));
    Point* freePoint;

    while(true)
    {
        freePoint = new Point((rand()%map->getX())*TEXTURE_SIZE, (rand()%map->getY())*TEXTURE_SIZE);

        //We test if the point isn't on a wall
        if(map->isWall((freePoint->getX()/TEXTURE_SIZE), (freePoint->getY()/TEXTURE_SIZE))) continue;


        //We test if the point isn't on the player
        if(player != NULL)
            if(player->getX() == freePoint->getX())
                if(player->getY() == freePoint->getY())
                    continue;

        //We test if the point isn't on the AIs
        if(ai != NULL)
        {
            bool isOnAi = false;
            for(int i = 0; i < NB_ENEMIES; i++)
            {
                if(ai[i] == NULL) break;

                if(ai[i]->getX() == freePoint->getX())
                    if(ai[i]->getY() == freePoint->getY())
                        isOnAi = true;
            }

            if(isOnAi) continue;
        }

        //if we went through all the test it's that the point is a free point, else we do again these operations
        return freePoint;
    }
    return NULL;
}

Player* SingleGame::nearestEnemy(int aiIndex)
{
    Player* ai;
    Player* res = NULL;

    for(int i = 0; i < NB_ENEMIES; i++)
    {
        ai = this->ai[i];
        if((i == aiIndex) || !ai->isVisible()) continue;

        if(res == NULL)
        	res = ai;

        if(res->getPosition()->distance(this->ai[aiIndex]->getPosition()) > ai->getPosition()->distance(this->ai[aiIndex]->getPosition())) 
        {
            res = ai;
        }
    }

    if(player->isVisible())
	    if(res == NULL || res->getPosition()->distance(this->ai[aiIndex]->getPosition()) > player->getPosition()->distance(this->ai[aiIndex]->getPosition()))
	    	res = player;

    return res;
}

void SingleGame::moveAi()
{
	sf::sleep(sf::milliseconds(SPEED_ENEMIES));
	char dir;
    Map* map = getMap();
    Player* ai;
    Player* near;
    Point goal;
    Point posAi;

    for(int i = 0; i < NB_ENEMIES; i++)
    {
        ai = this->ai[i];

        if(!ai->isVisible()) continue;
        
        near = nearestEnemy(i);

        if(near == NULL) break;
        
        goal = Point(near->getX()/TEXTURE_SIZE, near->getY()/TEXTURE_SIZE);
        posAi = Point(ai->getX()/TEXTURE_SIZE, ai->getY()/TEXTURE_SIZE);

        list<Point> goalWay = makeWay(AStar::way(map->getMap(), map->getY(), map->getX(), posAi, goal));

        if(goalWay.size() > 0 && ai->getX() % TEXTURE_SIZE == 0 && ai->getY() % TEXTURE_SIZE == 0)
        {
            goalWay.pop_front();
            goal = goalWay.front();
	        if(goal.getX() == posAi.getX() && goal.getY() == posAi.getY() - 1)		dir = 'Z';
	        if(goal.getX() == posAi.getX() && goal.getY() == posAi.getY() + 1)		dir = 'S';
	        if(goal.getX() == posAi.getX() - 1 && goal.getY() == posAi.getY())		dir = 'Q';
	        if(goal.getX() == posAi.getX() + 1 && goal.getY() == posAi.getY())		dir = 'D';
	        ai->setWay(dir);
        }

        if(goalWay.size() > 0 || ai->getX() % TEXTURE_SIZE != 0 || ai->getY() % TEXTURE_SIZE != 0)	ai->move();
        else
        {
            ai->setWay(dirEnemy(ai, near->getPosition()));
            ai->animY();
            ai->shoot();
        }

    }
}


list<Point> SingleGame::makeWay(list<Point> way)
{

    if(way.size() <= BULLET_RANGE)
    {
        bool sameX = true;
        bool sameY = true;
        list<Point>::reverse_iterator it = way.rend();

        Point goal = way.back();

        for(list<Point>::reverse_iterator it = way.rbegin(); it != way.rend(); it++)
        {
            Point curr = *it;

            if(curr.getX() != goal.getX())
            {
                sameX = false;
            }
            if(curr.getY() != goal.getY())
            {
                sameY = false;
            }
        }

        if(sameY || sameX)
            way.clear();
        
    }

    return way;

}

char SingleGame::dirEnemy(Player* player, Point* goal)
{
    char res = player->getWay();
    Point* posPlayer = player->getPosition();

    if(posPlayer->getX() == goal->getX())
    {
        if(posPlayer->getY() < goal->getY())
            res = 'S';
        else 
            res = 'Z';
    } else  if(posPlayer->getY() == goal->getY()){
        if(posPlayer->getX() < goal->getX())
            res = 'D';
        else 
            res = 'Q';
    }

    return res;
}

void SingleGame::display()
{
    window->clear();
    map->display(window);
    player->display(window, player->getAnimX(),player->getAnimY());
    player->getBullet()->display(window, 174, 192);
    for(int i = 0; i < NB_ENEMIES; i++)
    {
        ai[i]->getBullet()->display(window, 174, 192);
        ai[i]->display(window, ai[i]->getAnimX(),ai[i]->getAnimY());
    }
    window->display();
}

void SingleGame::handleCollision()
{
    Player* mainPlayer = this->player;

    for(int i = 0; i < NB_ENEMIES; i++)
    {
        Player* ai = this->ai[i];
        if(ai->isVisible())
        {
            Bullet* bullet = ai->getBullet();
            if(bullet->isVisible())
            {
                for(int j = 0; j < NB_ENEMIES; j++)
                {
                    if(this->isCollision(bullet, player))
                    {
                        deathHandler(bullet, player);
                    }
                    if(i == j) continue;
                    Player* collider = this->ai[j];
                    if(this->isCollision(bullet, collider))
                    {
                        deathHandler(bullet, collider);
                    }
                }
            }
        }
    }

    Bullet* playerBullet = player->getBullet();

    if(playerBullet->isVisible())
    {
        for(int i = 0; i < NB_ENEMIES; i++)
        {
            if(ai[i]->isVisible())
            {
                Player* collider = this->ai[i];
                if(this->isCollision(playerBullet, collider))
                {
                    deathHandler(playerBullet, collider);
                }
            }
        }
    }
}

bool SingleGame::isEnd()
{
    bool res = false;
    int inLife = 0;

    for(int i = 0; i < NB_ENEMIES; i++)
    {
        if(ai[i]->isVisible()) inLife++;
    }

    if(player->isVisible()) inLife++;

    return inLife <= 1 || !player->isVisible();
}

void SingleGame::deathHandler(Bullet* weapon, Player* collider)
{
    weapon->reinit();
    collider->updateLife(weapon->getAttack());
    weapon->getPlayer()->updateDamages();
    if(collider->getLife() <= 0)
    {
        weapon->getPlayer()->updateKills();
        collider->setVisible(false);
        collider->getBullet()->setVisible(false);

        if(collider == player || getRank() == 1)
        {
            displayEnd();
        }
    }
}

void SingleGame::displayEnd()
{
    sf::Font font;   
    sf::Text position;
    sf::Text kills;
    sf::Text damages;
    sf::Text experience;
    sf::Text level;
    sf::Text nextLevel;
    sf::Text keyToContinue;
    std::stringstream content;

    int nbExperience = (int)((4 * pow(player->getLevel(), 3) / 5) + 1);
    int xWindow = (map->getX()*TEXTURE_SIZE);
    int yWindow = (map->getY()*TEXTURE_SIZE);
    int experienceWon = (player->getKills() * player->getDamages()) + (NB_ENEMIES+2)-getRank();

    if(!font.loadFromFile(FONT)) window->close();

    sf::RectangleShape background(sf::Vector2f(xWindow, yWindow));
    sf::RectangleShape experienceBar(sf::Vector2f(((player->getExperience()*xWindow)/nbExperience+1)+1, 50));

    background.setFillColor(sf::Color(0, 0, 0, 150));
    background.setPosition(0,0);
    experienceBar.setFillColor(sf::Color(255, 0, 0));
    experienceBar.setPosition(0, 3*(yWindow/4));

    position.setFont(font);
    kills.setFont(font);
    damages.setFont(font);
    experience.setFont(font);
    level.setFont(font);
    nextLevel.setFont(font);
    keyToContinue.setFont(font);

    content << "#" << getRank();
    position.setString(content.str());
    content.str(std::string());

    content << "Ennemies killed : " << player->getKills();
    kills.setString(content.str());
    content.str(std::string());

    content << "Damages thrown : " << player->getDamages();
    damages.setString(content.str());
    content.str(std::string());

    content << "Level " << player->getLevel();
    level.setString(content.str());
    content.str(std::string());

    content << "Level " << player->getLevel()+1;
    nextLevel.setString(content.str());
    content.str(std::string());

    position.setPosition(xWindow/2-32, yWindow/4);
    kills.setPosition(xWindow/3,yWindow/3);
    damages.setPosition(xWindow/3, yWindow/3 - 26);
    level.setPosition(15, (3*yWindow/4)-102);
    nextLevel.setPosition(xWindow - 9*24, (3*yWindow/4)-102);

    position.setCharacterSize(32);
    kills.setCharacterSize(24);
    damages.setCharacterSize(24);
    level.setCharacterSize(50);
    nextLevel.setCharacterSize(50);

    bool keyPressed = false;

    window->draw(background);
    window->draw(position);
    window->draw(kills);
    window->draw(damages);
    window->display();

    sf::Texture texScreen = sf::Texture();
    texScreen.create(xWindow, yWindow);
    texScreen.update(*window);
    sf::Sprite screen = sf::Sprite(texScreen);

    while(!keyPressed)
    {
        window->clear();
        window->draw(screen);

        content << "Level " << player->getLevel();
        level.setString(content.str());
        content.str(std::string());

        content << "Level " << player->getLevel()+1;
        nextLevel.setString(content.str());
        content.str(std::string());

        window->draw(level);
        window->draw(nextLevel);

        if(experienceWon-- > 0)
        {
            player->updateExperience(1);
            experienceBar.setSize(sf::Vector2f(((player->getExperience()*xWindow)/nbExperience+1)+1, 100));
            if(player->getExperience() >= nbExperience)
            {
                player->setExperience(player->getExperience() % nbExperience);
                player->levelUp();
                nbExperience = (int)((4 * pow(player->getLevel(), 3) / 5) + 1);
            }
        }

        window->draw(experienceBar);
        window->display();

        while(experienceWon <= 0 && window->pollEvent(event))
        {
            switch (event.type)
            {
                // window closed
                case sf::Event::Closed:
                    window->close();
                    break;
                // key pressed
                case sf::Event::KeyPressed:
                    keyPressed = true;
                    break;
            }
        }
    }

    Parser::savePlayer(player);
}