#include "MultiGame.hpp"

MultiGame::MultiGame(string playerUUID, sf::RenderWindow* window) : Game(window)
{	
	database = new Mysql(HOST, LOGIN, PASSWORD, DBNAME);

	nbPlayers = NB_PLAYERS;

	onlinePlayers = new Player*[NB_PLAYERS];
	for(int i = 0; i < NB_PLAYERS; i++)
	{
		onlinePlayers[i] = NULL;
	}

    initOnlinePlayers(playerUUID);
    initMap();

    Point* position = getFreePoint();
    Player* playerFromFile = Parser::loadPlayer(playerUUID, position);

    if(playerFromFile == NULL)  player = new Player(playerUUID, position, 0, 0, 96, 128);
    else    player = playerFromFile;

    database->query("INSERT INTO players VALUES(?, ?, ?, ?, ?, ?, 0)", true);
    database->set(1, playerUUID);
    database->set(2, player->getPosition()->getX());
    database->set(3, player->getPosition()->getY());
    database->set(4, player->getPosition()->getX());
    database->set(5, player->getPosition()->getY());
    database->set(6, player->getLife());
    database->execute();
}

MultiGame::~MultiGame()
{
    try{
        database->query("DELETE FROM players WHERE uuid = ?", true);
        database->set(1, player->getUuid());
        database->execute();
    } catch(string e) {}
    

	for(int i = 0; i < NB_PLAYERS; i++)
	{
		if(onlinePlayers[i] != NULL)
			delete onlinePlayers[i];
	}
	delete [] onlinePlayers;
	delete(database);
}

void MultiGame::initMap()
{
    bool sendMap = true;

    for(int i = 0; i < NB_PLAYERS; i++)
    {
        if(onlinePlayers[i] != NULL)
        {
            sendMap = false;
        }
    }

    double temp = 0.0;
    sf::RectangleShape loadingBar(sf::Vector2f(0, 50));
    sf::Text percentage;
    sf::Font font;

    int xWindow = (map->getX()*TEXTURE_SIZE);
    int yWindow = (map->getY()*TEXTURE_SIZE);

    loadingBar.setFillColor(sf::Color(255, 0, 0));
    loadingBar.setPosition(0, 3*(yWindow/4));

    if(!font.loadFromFile(FONT)) window->close();

    percentage.setFont(font);
    percentage.setPosition(xWindow/2, yWindow/2);
    percentage.setFillColor(sf::Color(255, 255, 255));
    percentage.setCharacterSize(35);
    if(sendMap)
    {
        this->map->generateEffective();
        database->query("DELETE FROM map");
    }

    std::stringstream content;

    for(int i = 0; i < MAP_SIZE; i++)
    {
        if(sendMap)
        {
            database->query("INSERT INTO map VALUES(?, ?)", true);
            database->set(1, i);

            int xValue = 0;
            
            for(int j = 0; j < MAP_SIZE; j++)
            {
                xValue += map->getValue(i,j)*pow(2, j);
            }
            database->set(2, xValue);

            database->execute();
        }
        else
        {
            database->query("SELECT content FROM map WHERE x = ?", true);
            database->set(1, i);
            database->execute();
            database->next();

            int xValue = stoi(database->getColumn("content"));

            for(int j = 0; j < MAP_SIZE; j++)
            {
                this->map->setInt(i, j, xValue%2);
                xValue /= 2;
            }
        }

        window->clear();
        double percent = round(((temp++)/((double)MAP_SIZE))*10000)/100;
        content << percent << "%";
        percentage.setString(content.str());
        content.str(std::string());
        loadingBar.setSize(sf::Vector2f((percent*xWindow)/100, 100));
        window->draw(loadingBar);
        window->draw(percentage);
        window->display();
    }
}

Player** MultiGame::getOnlinePlayers()
{
    return this->onlinePlayers;
}

bool MultiGame::isEnd()
{
    bool allOnlinePlayersDead = true;
    for(int i = 0; i < NB_PLAYERS; i++)
    {
        if(onlinePlayers[i] != NULL)
            allOnlinePlayersDead = false;
    }

    if(allOnlinePlayersDead)
        displayEnd();

    return player->getLife() <= 0 || allOnlinePlayersDead;
}

void MultiGame::waitingRoom()
{
	int xWindow = (map->getX()*TEXTURE_SIZE);
    int yWindow = (map->getY()*TEXTURE_SIZE);
    sf::Sprite background;
	sf::RectangleShape rectangle(sf::Vector2f(xWindow, yWindow));
    rectangle.setFillColor(sf::Color(0, 0, 0, 150));
    map->display(window);
    window->draw(rectangle);
    sf::Texture mapScreen;
    mapScreen.create(map->getX()*TEXTURE_SIZE, map->getY()*TEXTURE_SIZE);
    mapScreen.update(*window);
    background.setTexture(mapScreen);
    std::stringstream content;

    sf::Font font;
    sf::Text cpt;
    sf::Text waiting;

    font.loadFromFile(FONT);

    cpt.setFont(font);
    waiting.setFont(font);

    waiting.setString("Waiting for players");

    cpt.setPosition(xWindow/2 + 10, yWindow/2 - 10);
    waiting.setPosition(xWindow/2 - 160, yWindow/4);

    cpt.setCharacterSize(TITLE_CHAR_SIZE);
    waiting.setCharacterSize(TITLE_CHAR_SIZE);

    while(getNbPlayers() != NB_PLAYERS && window->isOpen())
    {
    	while(window->pollEvent(event))
    	{
    		switch(event.type)
        	{
            	case sf::Event::Closed:
            		window->close();
                	break;
        	}
    	}
        window->clear();
        content << getNbPlayers() << "/" << NB_PLAYERS;
    	cpt.setString(content.str());
    	content.str(std::string());
    	initOnlinePlayers(player->getUuid());
        window->draw(background);
        window->draw(waiting);
        window->draw(cpt);
        window->display();
    }

    this->player;
}

void MultiGame::initOnlinePlayers(string uuid)
{
	database->query("SELECT * FROM players WHERE uuid != ?", true);
    database->set(1, uuid);
    database->execute();

	int index = 0;
	for(int i = 0; i < NB_PLAYERS; i++)
	{
		if(onlinePlayers[i] != NULL)
			delete onlinePlayers[i];
	}

	while(database->next())
	{
		onlinePlayers[index] = new Player(database->getColumn("uuid"), new Point(stoi(database->getColumn("x")), stoi(database->getColumn("y"))), 288, 0, 96, 128);
		index++;
	}
}

int MultiGame::getNbPlayers()
{
	int players = 1;

	for(int i = 0; i < NB_PLAYERS; i++)
	{
		if(onlinePlayers[i] != NULL)
			players++;
	}

	return players;
}

void MultiGame::updateOnlinePlayers()
{
	for(int i = 0; i < NB_PLAYERS; i++)
	{
		Player* onlinePlayer = onlinePlayers[i];

		if(onlinePlayer == NULL)
			continue;

        try
        {
            database->query("SELECT * FROM players WHERE uuid = ?", true);
            database->set(1, onlinePlayer->getUuid());
            database->execute();
            database->next();

            onlinePlayer->getPosition()->setX(stoi(database->getColumn("x")));
            onlinePlayer->getPosition()->setY(stoi(database->getColumn("y")));
            onlinePlayer->setSpritePosition(stoi(database->getColumn("x")), stoi(database->getColumn("y")));
            onlinePlayer->getBullet()->getPosition()->setX(stoi(database->getColumn("x_ball")));
            onlinePlayer->getBullet()->getPosition()->setY(stoi(database->getColumn("y_ball")));
            onlinePlayer->getBullet()->setSpritePosition(stoi(database->getColumn("x_ball")), stoi(database->getColumn("y_ball")));
            onlinePlayer->setLife(stoi(database->getColumn("life")));
            onlinePlayer->getBullet()->setVisible(stoi(database->getColumn("ball_is_visible")));
        }
        catch(string e)
        {
            delete(onlinePlayers[i]);
            onlinePlayers[i] = NULL;
        }
	}

	database->query("UPDATE players SET x = ?, y = ?, x_ball = ?, y_ball = ?, life = ?, ball_is_visible = ? WHERE uuid = ?", true);
	database->set(1, player->getX());
	database->set(2, player->getY());
	database->set(3, player->getBullet()->getX());
	database->set(4, player->getBullet()->getY());
	database->set(5, player->getLife());
    database->set(6, player->getBullet()->isVisible());
    database->set(7, player->getUuid());
	database->execute();
}

bool MultiGame::isActive()
{
	Mysql db(HOST, LOGIN, PASSWORD, DBNAME);
	db.query("SELECT x,y FROM players");
	db.next();
	if(db.getColumn("x") == 0 && db.getColumn("y") == 0)
	{
		return false;
	}
	return true;
}

void MultiGame::display()
{
	window->clear();
    map->display(window);
    player->display(window, 0, 0);
    player->getBullet()->display(window, 174, 192);
    for(int i = 0; i < NB_PLAYERS; i++)
    {
    	if(onlinePlayers[i] != NULL)
    	{
    		onlinePlayers[i]->getBullet()->display(window, 174, 192);
        	onlinePlayers[i]->display(window, 0, 0);
    	}
    }
    window->display();
}

Point* MultiGame::getFreePoint()
{
    srand(time(0));
    Point* freePoint = new Point(0, 0);

    while(true)
    {
    	delete(freePoint);
        freePoint = new Point((rand()%this->map->getX())*TEXTURE_SIZE, (rand()%this->map->getY())*TEXTURE_SIZE);

        //We test if the point isn't on a wall
        if(map->isWall((freePoint->getX()/TEXTURE_SIZE), (freePoint->getY()/TEXTURE_SIZE))) continue;

        //We test if the point isn't on the onlinePlayers
        if(this->onlinePlayers != NULL)
        {
            bool isOnPlayer = false;
            for(int i = 0; i < NB_PLAYERS; i++)
            {
                if(onlinePlayers[i] == NULL) continue;

                if(onlinePlayers[i]->getX() == freePoint->getX())
                    if(onlinePlayers[i]->getY() == freePoint->getY())
                        isOnPlayer = true;
            }

            if(isOnPlayer) continue;
        }

        //if we went through all the test it's that the point is a free point, else we do again these operations
        return freePoint;
    }
    return NULL;
}

void MultiGame::handleCollision()
{
    for(int i = 0; i < NB_PLAYERS; i++)
    {
        Player* onlinePlayer = this->onlinePlayers[i];
        if(onlinePlayer == NULL) continue;
        Bullet* bullet = onlinePlayer->getBullet();
        if(this->isCollision(bullet, player))
        {
            deathHandler(bullet, player);
        }
    }
}

void MultiGame::handleBulletMovement()
{
    player->getBullet()->move();
    if(this->mapCollision(player->getBullet()))
        player->getBullet()->reinit();
}

void MultiGame::deathHandler(Bullet* weapon, Player* collider)
{
    collider->updateLife(weapon->getAttack());
    if(collider->getLife() <= 0)
    {
        collider->setVisible(false);
        collider->getBullet()->setVisible(false);
        displayEnd();
    }
}

void MultiGame::displayEnd()
{
    database->query("DELETE FROM players WHERE uuid = ?", true);
    database->set(1, player->getUuid());
    database->execute();

    sf::Font font;   
    sf::Text sentenceEnd;


    int xWindow = (map->getX()*TEXTURE_SIZE);
    int yWindow = (map->getY()*TEXTURE_SIZE);

    if(!font.loadFromFile(FONT)) window->close();

    sf::RectangleShape background(sf::Vector2f(xWindow, yWindow));
    background.setFillColor(sf::Color(0, 0, 0, 150));
    background.setPosition(0,0);

    sentenceEnd.setFont(font);

    sentenceEnd.setString("End game");

    sentenceEnd.setPosition(xWindow/2, yWindow/2);

    sentenceEnd.setCharacterSize(32);

    bool keyPressed = false;

    window->draw(background);
    window->draw(sentenceEnd);
    window->display();

    sf::Texture texScreen = sf::Texture();
    texScreen.create(xWindow, yWindow);
    texScreen.update(*window);
    sf::Sprite screen = sf::Sprite(texScreen);

    while(!keyPressed)
    {
        while(window->pollEvent(event))
        {
            switch (event.type)
            {
                // window closed
                case sf::Event::Closed:
                    window->close();
                    break;
                // key pressed
                case sf::Event::KeyPressed:
                    keyPressed = true;
                    break;
            }
        }
    }
}
