#ifndef MULTI_GAME
#define MULTI_GAME

#include "../Game.hpp"
#include "../db/Mysql.hpp"

class MultiGame : public Game
{

public:
	/**
	 * Costructor of multi game
	 * \param string playerUUID
	 * \param sf::RenderWindow* window
	 */
	MultiGame(string playerUUID, sf::RenderWindow* window);

	/**
	 * Destructor of single game
	 */
	virtual ~MultiGame();

	/**
	 * Display the game on sfml window 
	 */
	void display();

	/**
	 * Manage the collision
	 */
	void handleCollision();

	/**
	 * Search a free point to initialize the first player position
	 * \return Point* 
	 */
	Point* getFreePoint();

	/**
	 * Manage the bullet movement
	 */
	void handleBulletMovement();

	/**
	 * Return the number of ai in life 
	 * \return int
	 */
	int getRank();

	/**
	 * Display a waiting room to wait the another player 
	 */
	void waitingRoom();

	/**
	 * Return the player's number
	 * \return int 
	 */
	int getNbPlayers();

	/**
	 * Return online players table 
	 * \return Player**
	 */
	Player** getOnlinePlayers();


	static bool isActive();

	/**
	 * Update online players 
	 */
	void updateOnlinePlayers();

	/**
	 * Initialize online players
	 * \param string uuid
	 */
	void initOnlinePlayers(string uuid);

	/**
	 * Initialize map 
	 */
	void initMap();

	/**
	 * Detect the end of the game
	 * \return bool
	 */
	bool isEnd();

	/**
	 * Manages the death of players
	 * \param Bullet* weapon
	 * \param Player* collider
	 */
	void deathHandler(Bullet* weapon, Player* collider);

	/**
	 * Displaye the end of the game, the player number kills, total damages, rank and experience 
	 */
	void displayEnd();

private:
	Mysql* database;
	Player** onlinePlayers = NULL;
	int nbPlayers;
};

#endif